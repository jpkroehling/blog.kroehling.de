---
layout: post
title: "Quick tip: do not miss another GitHub mention anymore!"
redirect_from: /quick-tip-do-not-miss-another-github-mention-anymore/
---
Protip: GitHub adds a few mail headers to the message envelopes. If you have a lot of activity on GitHub, you might get lost on what is an important notification and what can be seen later (or even ignored). Sample headers and values:


    X-GitHub-Sender: <who took the action>
    X-GitHub-Recipient: <your GitHub username>
    X-GitHub-Reason: <type of the notification>
    X-GitHub-Recipient-Address: <your-email@domain.tld>

So, just create a filter that checks for the Header `X-GitHub-Reason` with the value `mention` and put it into a `GitHub mentions` folder!
