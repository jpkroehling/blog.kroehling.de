---
layout: post
title: Getting critical updates before they reach the repositories
redirect_from: /getting-critical-updates-before-they-reach-the-repositories/
---
From time to time, a piece of software on a server of yours needs to be updated very quickly, due to some recently discovered security issue. On your professional production machines, this wouldn't be a problem, as having RHEL subscriptions means that you'll get those updates really quick (you are using RHEL on those servers, right? Right?).

But what about our little personal servers, running the latest Fedora? In the normal case, it takes a day or two for an update to reach the update channels for your machine, but if you absolutely need something quicker, you can check [Koji](http://koji.fedoraproject.org/koji/index), the Fedora build system.

The only catch is: the package stability might not be the same as the ones reaching you via the official update channels. But for some situations, it might be worth the try, specially because `yum downgrade` is your friend in case something wrong happens.

A concrete example on how to use it:

    yum install https://kojipkgs.fedoraproject.org//packages/openssl/1.0.1e/38.fc20/i686/openssl-1.0.1e-38.fc20.i686.rpm https://kojipkgs.fedoraproject.org//packages/openssl/1.0.1e/38.fc20/i686/openssl-libs-1.0.1e-38.fc20.i686.rpm https://kojipkgs.fedoraproject.org//packages/openssl/1.0.1e/38.fc20/i686/openssl-devel-1.0.1e-38.fc20.i686.rpm
