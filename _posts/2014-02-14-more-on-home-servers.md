---
layout: post
title: Email hosting at home
redirect_from: /more-on-home-servers/
---
*Update*: ArsTechnica ran a very nice series on self-hosting emails, with a selection of components similar to what I have here. As it's far more detailed and explanatory than what I have here, [check it there first]( http://arstechnica.com/information-technology/2014/02/how-to-run-your-own-e-mail-server-with-your-own-domain-part-1/)

I hinted earlier that I'm hosting my emails at home, and I think it's been now long enough to get a clear view on what has worked and what has not worked.

First, I'd like to point out that I'm not a professional systems administrator, but I like to think I'm a decent hobyist administrator.

One thing that clearly worked for me is that I'm most of the time working from home, so, my connection to get the emails is local, meaning that it's extremelly fast. Sending emails, even with big attachments is usually a matter of a couple seconds.

Another thing that worked quite well is that I've got a real IMAP server, not an emulated support (like Gmail's), with Sieve filters and all the niceties. I was really missing how better it was to use a real email client. And for the times that I need a webmail (ie: almost never), I've installed [RoundCube](http://roundcube.net/), which has all the basic features that I'd need on the road, plus a nice design. And it's available on Fedora's repository.

Additionally, I can easily add new domains, accounts and aliases, as it's mostly a simple SQL query away from the goal.

A few caveats though:

- I've found out that a few ISPs entirely blocks emails sent from "home" connections, bouncing it back to the originating server (ie: me). One other situation is that some other providers uses the fact that the email came from a "home" connection to increase the spam ranking (which is not wrong, actually). In the end, I've decided to make a compromise and setup an OpenVPN server at a VPS provider that gives me a fixed IP to the outside world. My choice was DigitalOcean, but could have been AWS as well. With that, I've got the best of two worlds: the world thinks that I'm in a "real" hosting environment, while it's in fact still at home.
- It goes without saying, but it requires a lot of work to setup an environment like this (at least, in a way that it's sustainable, with puppet, monitoring and so on). And it requires a couple of hours every week, to review the machine and update it. So, it's not for the average person: it's for those who love technology and love to mess around. Oh, and love to RTFM. It's not as hard as it was a decade ago (when qmail was pretty much the best option), but it's still not as easy as running a command or two. A good understanding of DNS (at least, how MX records work) and how email works in general is the basic that you should know. Those who wants things that "just work" should stick to their Macbooks using Gmail.

And finally, if you'd like to have a starting point, you can use my [Puppet scripts](https://bitbucket.org/jpkroehling/puppet-public/src/5a3b6c419680/production/modules/?at=master) as base.
