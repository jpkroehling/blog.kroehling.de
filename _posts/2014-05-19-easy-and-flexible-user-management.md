---
layout: post
title: Easy and flexible user management with Keycloak
redirect_from: /easy-and-flexible-user-management/
---
I've lost count on how many times I've done this:

- start a project
- build a login screen
- build the login procedure
- thought for a couple of minutes/hours/days about the password storage
- debugged the login screen
- debugged the login procedure
- build a "what's my password" feature
- build a "manage my account" feature
- integrated with social logins
- debugged the social logins
- started the actual project

And in the end, we are never sure if we missed something, specially related to [security](https://www.owasp.org/index.php/Top_10_2013-A2-Broken_Authentication_and_Session_Management). Reminder: broken auth and session management is on position 2 of OWASP's Top 10.

If this sounds like something you've done as well, I'd then recommend to take a look at the project [Keycloak](http://keycloak.jboss.org/). It's an open source software for managing the users of your application, with a whole lot of features that you'd expect from a tool like this.

Granted, the tools is still in alpha, but I'm using it already for a couple of personal projects, with great success. Of special mention is the ability to easily detatch your front end to your backend, with the usage of JSON Web Tokens: with it, you don't need to keep a state at all on the server side, usually a problem with APIs whose main consumer is a front end application.

And if your application happens to be hosted on a Wildfly/JBoss AS, you can use the Keycloak subsystem and have your application to be agnostic of the authentication/authorization procedores. It's [JAAS](http://en.wikipedia.org/wiki/Java_Authentication_and_Authorization_Service) being cool again :-)
