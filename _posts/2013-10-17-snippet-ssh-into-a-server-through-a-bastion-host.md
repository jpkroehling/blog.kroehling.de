---
layout: post
title: "Snippet: ssh into a server through a bastion host"
redirect_from: /snippet-ssh-into-a-server-through-a-bastion-host/
---
    Host farawayserver
    User <your_username_on_farawayserver>
    ForwardAgent yes
    ProxyCommand ssh -A -W <ip-of-farawayserver>:%p <server-to-use-as-bastion>

Example:

    Host boitata
    User jpkroehling
    ForwardAgent yes
    ProxyCommand ssh -A -W 192.168.122.30:%p acai
