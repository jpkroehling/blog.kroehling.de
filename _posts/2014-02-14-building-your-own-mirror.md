---
layout: post
title: Building your own mirror
redirect_from: /building-your-own-mirror/
---
When hosting services at home, one common scenario is that all the VMs have the same OS and the same basic software, with a few differences between them. This means that, if you have 7 VMs (as it's my case), you'd download the same updates 7 times. While this is not a big problem for the day-to-day, it becomes a hassle when you decide to upgrade your VMs. Downloading hundred of packages, or even booting a new VM, becomes a task that takes some time to finish.

Tired of this, I've decided to build a simple mirroring server. I'm using nginx and squid for that. Simply put, that's all one needs:

For nginx, on Fedora 20:

    yum install nginx squid
    firewall-cmd --add-port=80/tcp --permanent
    firewall-cmd --add-port=443/tcp --permanent
    firewall-cmd --add-port=80/tcp
    firewall-cmd --add-port=443/tcp

And on `/etc/squid/squid.conf`:

    maximum_object_size 4 GB
    http_port 3128 accel defaultsite=dl.fedoraproject.org
    cache_peer dl.fedoraproject.org parent 80 0 no-query originserver
    http_access allow all
    cache_dir ufs /var/spool/squid 161166 16 256
    access_log /var/log/squid/access.log

On `/etc/nginx/conf.d/mirror.conf`:

    server {
        listen 80;
        server_name mirror.kroehling.de;
        location / {
            proxy_set_header X-Forwarded-Host $host;
            proxy_set_header X-Forwarded-Server $host;
            proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
            proxy_pass http://localhost:3128/;
        }
    }

After that, all you need is to point your machines to use this host when getting updates. This can be done by changing the `/etc/yum.repos.d/fedora*repo` files, to use the URL from your mirror as `baseurl`, instead of using a `metalink`.

And when you decide to boot new VMs, you can also take advantage of this mirror, using the "-l" parameter on `virt-install`, pointing to the `os` directory on your mirror, like this:

    virt-install --ram 1024 -l http://mirror.kroehling.de/pub/fedora/linux/releases/20/Fedora/x86_64/os/ --os-type=linux --os-variant=fedora20 --disk path=/usr/local/data/projects/virtual-images/NAME.qcow2,format=qcow2 --vnc --name NAME
