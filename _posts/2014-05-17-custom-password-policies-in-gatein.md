---
layout: post
title: Custom password policies in GateIn
redirect_from: /custom-password-policies-in-gatein/
---
GateIn comes with a set of rules for validating the usernames and passwords, but it might be the case that you want to tweak the password policy to follow your company's policy.

If that's the case, GateIn 3.8.1 and higher allows you to specify the parameters for your own policy. On this release, there's a new "special" policy called "passwordpolicy", which you can specify in your conf/configuration.properties.

    gatein.validators.passwordpolicy.format.message=Minimum of 1 digit, 1 lower case, 1 upper case, minimum of 6 chars, max of 20.
    gatein.validators.passwordpolicy.regexp=((?=.*\\d)(?=.*[a-z])(?=.*[A-Z]).{6,20})
    gatein.validators.passwordpolicy.length.max=20
    gatein.validators.passwordpolicy.length.min=6

Note, however, that you'll need to restart the server whenever there's a change in the policy, which is a fair price to pay, as policies don't change that often.
