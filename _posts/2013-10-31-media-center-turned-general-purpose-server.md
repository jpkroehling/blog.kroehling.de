---
layout: post
title: Media Center turned general purpose server
redirect_from: /media-center-turned-general-purpose-server/
---
Some time ago, I was in the market for a new HTPC for my living room. A quick look at the machines available on the market today made it clear to me that building your own machine gives a better bang for the buck. For about 50 EUR more, it's possible to build a far better machine, with the previous generation of Intel processors. After I decided to build my own, I started considering that I might indeed spend a bit more, and build a machine that will last for several years as Media Center, hopefully being still useful in about 4 years, when I expect to buy the first 4k TV. Granted, not everyone would be willing to spend almost 450 EUR on a Media Center machine, but this is how it ended up:

* Intel Haswell i5 (4570S)
* ASUS Z87i-Pro (C2)
* Cooler Master Mini 110
* Sandisk 64GB SSD
* Kingston 4GB RAM

I had already a 1TB external USB 3.0 disk and a remanescent 2.5' 256GB HDD from my old HTPC, so, the new SSD was totally optional, but certainly welcomed: booting Fedora 19 on it takes less than 2 seconds, directly into XBMC.

As the machine became very powerful for a Media Center for today's standards, I've decided to borrow some of the power from the future to replace some services that I use from cloud providers nowadays: Jabber, ownCloud and OpenPhoto, which were stuffed into my mail server at EC2, as well a Java EE application that was on OpenShift and Jenkins, that was on CloudBees. As a bonus, I installed an Artifactory server. With 30 EUR more for an extra 4GB of RAM, and I could host pretty much everything I wanted/needed there.

At first, they were all on the main hardware, but it soon became clear that having everything on the same OS would become problematic. So, the next natural step was to build some VMs and run the services on those VMs. As it currently is, I have 7 VMs:

* Database server, with PostgreSQL and MariaDB
* JBoss AS server
* Jenkins slave
* Apache with ownCloud and OpenPhoto, and Tomcat server, with Jenkins master and Artifactory
* Jabber server
* Puppet server (I certainly don't want to manage all of them manually)
* Experimenting server, for installing/building packages on my own

Pretty much all of the VMs have 256MB of RAM, except the Jenkins slave and the JBoss AS, with 512MB each and the Apache/Tomcat, with 2GB. The last one will be reduced, once I split the Tomcat server into a VM as well.

The good thing about all of this is that I got a good practical knowledge on KVM/qemu and libvirt, to the point that I'm quite comfortable into doing the bootstrap/maintenance from remote (virsh rocks, btw). This is an area that I didn't have any experience before, and I'm quite happy to have had the opportunity to learn it.

The bad part is that I probably spent too much money (~500 EUR) for a machine whose main purpose is to just play media.

But all in all, the only regret is that I should have had bought a single 8GB from the beginning. If I ever decide to expand the memory again, I'd have to either throw away both and buy two 8GB, or live with a mix of 4GB and 8GB.
