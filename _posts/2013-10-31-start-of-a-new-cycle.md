---
layout: post
title: Start of a new cycle
redirect_from: /start-of-a-new-cycle/
---
Some years ago, I've got a call from a recruiter looking for a QA Engineer for a company in the Czech Republic. I always wanted to live there, so, it felt like a good opportunity. But having a development background made me a bit anxious: would it be technical enough? A few days after I started at Red Hat, in Brno, I've learned that it can indeed be extremelly technical.

Being a QA Engineer means that one needs to get deep into the code and find issues that weren't obvious to those writing the code. It means that you have to run the code in your head, looking for ways to break it. Instead of asking "would it work?", you'd start asking "how can I make it *not* work?".

It is indeed extremely fun to work as a QA Engineer.

A few years have passed, and I've gotten the opportunity to join Red Hat again, this time as a developer. At first, I thought that I would be "going back to development", but in fact, I don't think I've ever stopped being a developer. Reading about new techniques, languages, frameworks, architectures are part of the job for both developers and QA Engineers. And coding is something that I do in my free time. In any case, I feel like a cycle has come to an end, and a new cycle starts.
