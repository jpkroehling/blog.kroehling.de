---
layout: post
title: "Quick tip: Auto create JMS queues on Wildfly 10"
redirect_from: /quick-tip-auto-create-jms-queues-on-wildfly-10/
---
Wildfly 10 ships with ActiveMQ Artemis as the default JMS implementation, but has the `auto-create-jms-queues` set to false by default (to keep the compatibility with older Wildfly versions). To enable it back, change the `<address-setting>` node on `standalone.xml` to include `auto-create-jms-queues="true"`:

    <address-setting auto-create-jms-queues="true" name="#" dead-letter-address="jms.queue.DLQ"... />
