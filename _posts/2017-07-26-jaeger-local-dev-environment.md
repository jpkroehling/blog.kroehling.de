---
layout: post
title: "Quick tip: Cassandra for Jaeger development"
---
When preparing a local development environment for Jaeger, it might be needed to spin up a new Cassandra cluster.
Instead of starting it locally, one option is to start via Docker, so that it can be completely destroyed after use, like this:

    docker run --rm -it --name cassandra -p 9042:9042 cassandra

Then, we need to create the schema. From the root of the Jaeger's source repository, this can be executed:

    MODE=test KEYSPACE=jaeger_v1_local ./plugin/storage/cassandra/schema/create.sh plugin/storage/cassandra/schema/v001.cql.tmpl | docker exec -i cassandra cqlsh

