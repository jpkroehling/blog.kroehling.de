---
layout: post
title: Fedora 19 and Forward Secrecy
redirect_from: /fedora-19-and-forward-secrecy/
---
Update: It seems that Fedora 19 has now shipped with (usable) support for Forward Secrecy! See this: https://bugzilla.redhat.com/show_bug.cgi?id=319901#c108

Original text:

After reading [(and listening)](http://twit.tv/sn) a bit about security, you might have heard about "Perfect Forward Secrecy" or just "Forward Secrecy" (as nothing is really perfect in crypto). But to achieve that with Fedora is a bit of a challenge, because there's some fear that the algorithms used in the best FS scenarios might be patent encumbered. As per Fedora's guidelines, this is a blocker and cannot be made available from the official repositories.

Forward Secrecy is increasingly important to prevent that a third-party gets access to the plain texts in case of leakeage of the private key, specially considering some recent events.

To achieve Forward Secrecy on Fedora, then, you'll need to compile the openssl-dev package and all packages that were compiled with it, such as httpd, postfix, dovecot, openvpn and so on. The following instructions are related to Apache's HTTP server, but can be easily adapted to work with the other services.

First of all, you'll need to compile `openssl-dev` with ECDHE and DHE support. This is well explained in a [blog post from Daniel Pocock](http://danielpocock.com/ussing-ecc-ecdsa-in-openssl-and-strongswan-fedora), reproduced (and adapted) here:

    sudo su -
    rpm -e openssl-devel
    yum install rpm-build krb5-devel zlib-devel gcc \
       gmp-devel libcurl-devel openldap-devel \
       NetworkManager-devel NetworkManager-glib-devel sqlite-devel
    [ ! -e ~/.rpmmacros ] && \
        echo '%_topdir      %(echo $HOME)/rpmbuild' > ~/.rpmmacros
    [ ! -d rpmbuild ] && mkdir rpmbuild
    cd ~/rpmbuild
    mkdir -p BUILD BUILDROOT RPMS/i386 RPMS/x86_64 SOURCES SPECS SRPMS
    cd ~/rpmbuild/SRPMS
    wget http://dl.fedoraproject.org/pub/fedora/linux/releases/19/Everything/source/SRPMS/o/openssl-1.0.1e-4.fc19.src.rpm
    rpm -i openssl-1.0.1e-4.fc19.src.rpm
    cd ../SOURCES
    wget http://www.openssl.org/source/openssl-1.0.1e.tar.gz
    cd ../SPECS
    wget http://zxvdr.fedorapeople.org/openssl.spec.ec_patch
    # Fedora's SRPM has a modified source, must use the original
    patch -p0 < openssl.spec.ec_patch
    sed -i -e 's/-usa.tar.xz/.tar.gz/' openssl.spec
    rpmbuild -bb openssl.spec

After these steps, you'll end up with the new packages at `/root/rpmbuild/RPMS/x86_64` . If you already have the original packages installed, you will need to change the file `openssl.spec` and bump the `Release` number, like this:

    Release: 5%{?dist}

With that, your package would be named `openssl-devel-1.0.1e-5.fc19.x86_64.rpm`.
Then, you'll need to install them:

    yum install /root/rpmbuild/RPMS/x86_64/openssl*rpm

After this, you'll be ready to compile your dependent packages, like httpd. Note that you might need to check the latest version of the package on the repository, which happens to be the following at the time of this writing:

    wget http://dl.fedoraproject.org/pub/fedora/linux/updates/19/SRPMS/httpd-2.4.6-2.fc19.src.rpm
    yum install autoconf xmlto lua-devel apr-devel \
        apr-util-devel pcre-devel systemd-devel libxml2-devel
    rpm -i httpd-2.4.6-2.fc19.src.rpm
    rpmbuild -bb httpd.spec

Similarly, if you already have `httpd` installed, you'll need to bump the `Release`, like this:

    Release: 3%{?dist}

After that, you'll end up with `httpd` and `mod_ssl` packages (among others) on `/root/rpmbuild/RPMS/x86_64/`. You'll then just need to install them:

    yum install /root/rpmbuild/RPMS/x86_64/httpd*rpm /root/rpmbuild/RPMS/x86_64/mod_ssl*rpm

Once you install them, you can test your HTTP server on SSL Labs and [get a result similar to this one](https://www.ssllabs.com/ssltest/analyze.html?d=mail.kroehling.de).

Final note: you might be able to find pre-compiled versions of those packages on the Internet, but I'd strongly advise *not* to install those. If you care about providing Forward Secrecy, you don't want to trust some random person's version of OpenSSL, Apache's HTTP server, OpenVPN and so on. Really.
