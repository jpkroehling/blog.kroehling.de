---
layout: post
title: Docker images for Keycloak
redirect_from: /docker-images-for-keycloak/
---
I mentioned [Keycloak](http://www.jboss.org/keycloak) on a previous note, and I thought it would be interesting to share two Docker images that were just published. So, if you want to give Keycloak a try, all you need to do is this:

    $ docker run -it -p 8080:8080 jpkroehling/keycloak-server

Once it pulls [the server image](https://index.docker.io/u/jpkroehling/keycloak-server/) and boots the server, you can login with the `admin/admin` credentials on [the admin interface](http://localhost:8080/auth) (you'll have to change the password on the first access, for obvious reasons).

The [second Docker image](https://index.docker.io/u/jpkroehling/keycloak-examples/) is based on the first one, and provides some sample applications to show how Single Sign On between a multitude of clients can be achieved. You can refer to the Keycloak docs for details on the examples, but here's how to get started:

    $ docker run -it -p 8080:8080 jpkroehling/keycloak-examples

Additionally to the server image, this allows you to login to the [Customer Portal sample application](http://localhost:8080/customer-portal/customers/view.jsp) using the credentials mentioned on the documentation (bburke@redhat.com / password).

As you'll be able to see, you can use Keycloak even with non-Java applications, like HTML5 single-page apps or applications with different backends (PHP, Ruby, ...).

Enjoy!
