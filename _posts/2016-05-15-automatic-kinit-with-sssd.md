---
layout: post
title: Automatic kinit with sssd
redirect_from: /automatic-kinit-with-sssd/
---
I came across [this post](https://jhrozek.wordpress.com/2015/07/17/get-rid-of-calling-manually-calling-kinit-with-sssds-help/) from my Red Hat colleague Jakub Hrozek and I thought it was fantastic: automatically get a Kerberos ticket once you login to your computer. This means that I don't need to `kinit` from time to time anymore!

While his instructions are great, I'm not that used to `sssd`, so, I had some difficulties in applying the instructions. Here's what I found that his post is missing:

- `sssd.conf` should be placed `/etc/sssd/sssd.conf`
- Don't forget to `systemctl start sssd` and `systemctl enable sssd`
- The first `kinit` has to be done manually, so that it's cached

That's it!
