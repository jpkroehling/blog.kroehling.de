---
layout: post
title: Wait on conditions, not time
redirect_from: /wait-on-conditions-not-time/
---
For operations that trigger actions on the background, it's tempting to add a `Thread.sleep()` or a synchronized `wait()` for a fixed amount of time. The best is to always use a `Future<>` whenever possible, which usually means "whenever the API allows". For the cases where the API you are consuming doesn't provide a `Future<>` for you to wait, you can implement a poor-man's `Wait` helper, like the following:

    public static void until(Callable<Boolean> condition) {
        FutureTask<Void> futureTask = new FutureTask<Void>(() -> {
            while (!condition.call()) {
                Thread.sleep(50);
            }
            return null;
        });

        ExecutorService executor = Executors.newFixedThreadPool(1);
        executor.submit(futureTask);
        try {
            futureTask.get(10, TimeUnit.SECONDS);
        } catch (InterruptedException | ExecutionException | TimeoutException e) {
            futureTask.cancel(true);
            e.printStackTrace();
        }
    }

And consume it like this:

    Wait.until(() -> myCondition);

This will evaluate the condition every 50ms and return once the condition is met, throwing an exception if it takes longer than 10 seconds.
