---
layout: post
title: Injecting a ServletContext into a WebSocket
redirect_from: /injecting-a-servletcontext-into-a-websocket/
---
Perhaps because of an oversight, it's not possible to directly get deployment-related information from a Java EE WebSocket server endpoint. So, init parameters and alike cannot be directly obtained via a WebSocket ServerEndpoint.

Luckily, CDI can be used to overcome this. The idea is to implement a ServletContextListener that gets an @ApplicationScoped bean and sets the ServletContext there. Then, our WebSocket server endpoint can just inject our @ApplicationScoped bean.

	public class ServletContextEnhancer implements ServletContextListener {
	    @Inject
	    ApplicationResources applicationResources;

	    @Override
	    public void contextInitialized(ServletContextEvent servletContextEvent) {
	        applicationResources.setServletContext(servletContextEvent.getServletContext());
	    }

	    @Override
	    public void contextDestroyed(ServletContextEvent servletContextEvent) {
	    }

	}

	@ApplicationScoped
	public class ApplicationResources {
	    private String myConfiguration = null;
	    private ServletContext servletContext;

	    public void setServletContext(ServletContext servletContext) {
	        this.servletContext = servletContext;
	    }

	    @Produces @MyConfiguration
	    public String getMyConfiguration() {
	        if (null == myConfiguration) {
	            myConfiguration = servletContext.getInitParameter("myConfiguration");
	        }
	        return myConfiguration;
	    }
	}

	@ServerEndpoint(value = "/socket")
	public class Socket {
	    @Inject @MyConfiguration
	    String myConfiguration;

	    @OnMessage
	    public String onMessage(String message, Session session) {
	        return message + myConfiguration;
	    }
	}
